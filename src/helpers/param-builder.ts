import { SortArgs } from 'nestjs-graphql-tools';

export const buildSortParams = <T>(sorting: SortArgs<T>) => {
  const sortingParams = {};
  for (const p in sorting) {
    sortingParams[p.replace('t.', '')] = sorting[p].toString();
  }
  console.log(sortingParams);
  return sortingParams;
};
