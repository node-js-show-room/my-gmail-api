import { Field, ID, ObjectType } from '@nestjs/graphql';
import {
  Entity,
  Column,
  ObjectIdColumn,
  UpdateDateColumn,
  CreateDateColumn,
} from 'typeorm';

@Entity()
@ObjectType()
export class MailMessage {
  @Field(() => ID)
  @ObjectIdColumn()
  _id: string;

  @Field(() => String)
  @Column()
  body: string;

  @Field(() => String)
  @Column()
  subject: string;

  @Field(() => Boolean, {
    nullable: true,
    defaultValue: false,
  })
  @Column({
    default: false,
  })
  deleted!: boolean;

  @Field(() => Date, {
    nullable: true,
  })
  @CreateDateColumn()
  created!: Date;

  @Field(() => Date, {
    nullable: true,
  })
  @UpdateDateColumn()
  updated!: Date;
}
