import { Test, TestingModule } from '@nestjs/testing';
import { MailMessageService } from './mail-message.service';

describe('MailMessageService', () => {
  let service: MailMessageService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [MailMessageService],
    }).compile();

    service = module.get<MailMessageService>(MailMessageService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
