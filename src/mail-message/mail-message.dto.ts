import { Field, InputType } from '@nestjs/graphql';
// import { IsEmail, IsNotEmpty, IsString } from 'class-validator';

@InputType()
export class MailMessageInput {
  @Field(() => String)
  body: string;

  @Field(() => String)
  subject?: string;
}
