import { Inject } from '@nestjs/common';
import { Args, Mutation, Query, Resolver, Subscription } from '@nestjs/graphql';
import { PubSub, PubSubEngine } from 'graphql-subscriptions';
import { GraphqlSorting, SortArgs, Sorting } from 'nestjs-graphql-tools';
import { MailMessageInput } from './mail-message.dto';
import { MailMessage } from './mail-message.entity';
import { MailMessageService } from './mail-message.service';

const mailMessageAdded = 'mailMessageAdded';

@Resolver('MailMessage')
export class MailMessageResolver {
  constructor(
    private readonly mailMessageService: MailMessageService,
    @Inject('PUB_SUB') private pubSub: PubSubEngine,
  ) {}

  @Query(() => [MailMessage])
  @GraphqlSorting()
  async mailMessages(
    @Sorting(() => MailMessage)
    sorting: SortArgs<MailMessage>,
  ): Promise<MailMessage[]> {
    return this.mailMessageService.findAll(sorting);
  }

  @Mutation(() => MailMessage)
  async createMailMessage(
    @Args('input') input: MailMessageInput,
  ): Promise<MailMessage> {
    const newMailMessage = await this.mailMessageService.create(input);
    this.pubSub.publish(mailMessageAdded, {
      mailMessageAdded: newMailMessage,
    });
    return newMailMessage;
  }

  @Subscription((returns) => MailMessage)
  mailMessageAdded() {
    return this.pubSub.asyncIterator(mailMessageAdded);
  }
}
