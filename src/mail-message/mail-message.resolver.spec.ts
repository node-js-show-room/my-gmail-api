import { Test, TestingModule } from '@nestjs/testing';
import { MailMessageResolver } from './mail-message.resolver';

describe('MailMessageResolver', () => {
  let resolver: MailMessageResolver;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [MailMessageResolver],
    }).compile();

    resolver = module.get<MailMessageResolver>(MailMessageResolver);
  });

  it('should be defined', () => {
    expect(resolver).toBeDefined();
  });
});
