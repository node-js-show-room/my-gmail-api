import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { PubSub } from 'graphql-subscriptions';
import { MailMessage } from './mail-message.entity';
import { MailMessageResolver } from './mail-message.resolver';
import { MailMessageService } from './mail-message.service';

@Module({
  imports: [TypeOrmModule.forFeature([MailMessage])],
  providers: [
    MailMessageResolver,
    MailMessageService,
    {
      provide: 'PUB_SUB',
      useValue: new PubSub(),
    },
  ],
})
export class MailMessageModule {}
