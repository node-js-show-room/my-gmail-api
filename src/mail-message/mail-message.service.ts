import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { MongoRepository } from 'typeorm';
import { MailMessageInput } from './mail-message.dto';
import { MailMessage } from './mail-message.entity';
import * as uuid from 'uuid';
import { SortArgs } from 'nestjs-graphql-tools';
import { buildSortParams } from 'src/helpers/param-builder';

@Injectable()
export class MailMessageService {
  constructor(
    @InjectRepository(MailMessage)
    private readonly mailMessageRepository: MongoRepository<MailMessage>,
  ) {}

  async findAll(sorting: SortArgs<MailMessage>): Promise<MailMessage[]> {
    return this.mailMessageRepository.find({
      order: {
        ...(sorting || null),
      },
    });
  }

  async create(input: MailMessageInput): Promise<MailMessage> {
    const mailMessage = new MailMessage();
    mailMessage._id = uuid.v4();
    mailMessage.body = input.body;
    mailMessage.subject = input.subject;
    return this.mailMessageRepository.save(mailMessage);
  }

  async delete(id: string): Promise<boolean> {
    try {
      const mailMessage = await this.mailMessageRepository.findOneBy(id);
      mailMessage.deleted = true;
      this.mailMessageRepository.save(mailMessage);
      return true;
    } catch (error) {
      return false;
    }
  }

  async update(
    id: string,
    input: Partial<MailMessageInput>,
  ): Promise<MailMessage> {
    let mailMessage = await this.mailMessageRepository.findOneBy(id);
    mailMessage = {
      ...mailMessage,
      ...input,
    };
    return this.mailMessageRepository.save(mailMessage);
  }
}
